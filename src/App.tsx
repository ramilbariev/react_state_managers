import MainLayout from 'modules/songs/ui/layouts/mainLayout';
import React from 'react';
import AppRouter from './modules/router';

function App() {
  return (
    <MainLayout>
      <AppRouter />
    </MainLayout>
  );
}

export default App;
