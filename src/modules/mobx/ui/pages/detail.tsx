import React, { useCallback, useEffect } from 'react';
import mobxSongsStore from 'modules/mobx/store';
import { useParams } from 'react-router-dom';
import UDLoader from 'modules/ud-ui/components/ud-loader';
import SongsComponentsEditForm from 'modules/songs/ui/components/edit-form';

type Props = {
  store: typeof mobxSongsStore;
};

const MobxSongsDetailPage = (props: Props) => {
  const { store } = props;
  const { id = 1 } = useParams() as { id: number | undefined };
  useEffect(() => {
    store.loadById(+id);
  }, [id]);
  const onSubmit = useCallback((updated) => {
    store.update(updated);
  }, []);
  const song = store.detail;
  return (
    <div>
      <h1>MobxSongsDetailPage</h1>
      {store.isLoading && <UDLoader />}
      <h1>Edit</h1>
      {song && <SongsComponentsEditForm song={song} onSubmit={onSubmit} />}
    </div>
  );
};

export default MobxSongsDetailPage;
