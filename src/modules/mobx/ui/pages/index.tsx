import React, { useCallback, useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import UDLoader from 'modules/ud-ui/components/ud-loader';
import SongsComponentsCreateForm from 'modules/songs/ui/components/create-form';
import SongsComponentsSongsList from 'modules/songs/ui/components/list';
import mobxSongsStore from 'modules/mobx/store';

type Props = {
  store: typeof mobxSongsStore;
};

const MobxSongsIndexPage = observer((props: Props) => {
  const { store } = props;
  useEffect(() => {
    store.load();
  }, []);
  const onCreate = useCallback((data) => {
    store.create(data);
  }, []);
  const onRemove = useCallback((song) => {
    store.remove(song);
  }, []);
  return (
    <div>
      <h1>MobxSongsIndexPage</h1>
      {store.isLoading && <UDLoader />}
      <SongsComponentsCreateForm onSubmit={onCreate} />
      <SongsComponentsSongsList songs={store.songs} onRemove={onRemove} />
    </div>
  );
});

export default MobxSongsIndexPage;
