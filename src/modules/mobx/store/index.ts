import { makeAutoObservable, observable, action } from 'mobx';
import { Song } from 'modules/songs/domain/interfaces/Song';
import songsRepository from 'modules/songs/domain/repositories/songsRepository';

class MobxSong {
  id = null;
  name = '';
  author = '';
  release_year = '';

  constructor() {
    makeAutoObservable(this);
  }
}

class MobxSongsStore {
  songs = [] as Song[];
  detail = undefined as Song | undefined;
  isLoading = false;

  constructor(songs: Song[]) {
    makeAutoObservable(this);
    this.songs = songs;
  }

  setLoading(value: boolean) {
    this.isLoading = value;
  }

  load() {
    this.isLoading = true;
    songsRepository
      .load()
      .then(
        action('fulfilled', (songs) => {
          this.songs = songs;
        })
      )
      .finally(
        action('endAsync', () => {
          this.isLoading = false;
        })
      );
  }

  loadById(id: number) {
    this.isLoading = true;
    songsRepository
      .loadById(id)
      .then(
        action('fulfilled', (song) => {
          if (song) {
            this.detail = song;
            this.songs = [...this.songs.filter((song) => song.id !== id), song];
          }
        })
      )
      .finally(
        action('endAsync', () => {
          this.isLoading = false;
        })
      );
  }

  create(data: Omit<Song, 'id'>) {
    this.isLoading = true;
    songsRepository
      .create(data)
      .then(
        action('fulfilled', (created) => {
          this.songs.push(created);
        })
      )
      .finally(
        action('endAsync', () => {
          this.isLoading = false;
        })
      );
  }

  remove(songToRemove: Song) {
    this.isLoading = true;
    songsRepository
      .delete(songToRemove)
      .then(
        action('fulfilled', () => {
          this.songs = this.songs.filter((song) => song.id !== songToRemove.id);
        })
      )
      .finally(
        action('endAsync', () => {
          this.isLoading = false;
        })
      );
  }

  update(song: Song) {
    this.isLoading = true;
    songsRepository
      .update(song)
      .then(
        action('fulfilled', (updatedSong) => {
          const songWithoutUpdated = this.songs.filter((song) => song.id !== updatedSong.id);
          this.songs = [updatedSong, ...songWithoutUpdated];
        })
      )
      .finally(
        action('endAsync', () => {
          this.isLoading = false;
        })
      );
  }
}

const mobxSongsStore = new MobxSongsStore([]);
export default mobxSongsStore;
