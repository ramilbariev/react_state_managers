import React, { useEffect } from 'react';
import UDLoader from 'modules/ud-ui/components/ud-loader';
import SongsComponentsEditForm from 'modules/songs/ui/components/edit-form';
import { useCallback } from 'react';
import { Song } from 'modules/songs/domain/interfaces/Song';
import { useDispatch, useSelector } from 'react-redux';
import {
  reduxObservableSongDetailSelector,
  reduxObservableIsLoadingSelector,
} from 'modules/redux-observable/store/selectors';
import actionsCreators from 'modules/redux-observable/store/actions/actionCreators';
import { useParams } from 'react-router-dom';

type Props = {};

const ReduxObservableModuleSongDetailPage = (props: Props) => {
  const dispatch = useDispatch();
  const { id = 1 } = useParams() as { id: number | undefined };
  useEffect(() => {
    dispatch(actionsCreators.loadById.pending(+id));
  }, [id]);
  const onSubmit = useCallback((data) => {}, []);
  const song = useSelector(reduxObservableSongDetailSelector);
  const isLoading = useSelector(reduxObservableIsLoadingSelector);
  return (
    <div>
      <h1>ReduxObservableModuleSongDetailPage</h1>
      {isLoading && <UDLoader />}
      <h1>Edit</h1>
      {song && <SongsComponentsEditForm song={song} onSubmit={onSubmit} />}
    </div>
  );
};

export default ReduxObservableModuleSongDetailPage;
