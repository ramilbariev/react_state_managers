import React from 'react';
import SongsComponentsCreateForm from 'modules/songs/ui/components/create-form';
import SongsComponentsSongsList from 'modules/songs/ui/components/list';
import UDLoader from 'modules/ud-ui/components/ud-loader';
import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import actionsCreators from 'modules/redux-observable/store/actions/actionCreators';
import {
  reduxObservableIsLoadingSelector,
  reduxObservableSongsSelector,
} from 'modules/redux-observable/store/selectors';

type Props = {};

const ReduxObservableModuleIndexPage = (props: Props) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actionsCreators.load.pending());
  }, []);
  const isLoading = useSelector(reduxObservableIsLoadingSelector);
  const songs = useSelector(reduxObservableSongsSelector);
  const onCreate = useCallback((data) => {}, []);
  const onRemove = useCallback((data) => {}, []);
  return (
    <div>
      <h1>ReduxObservableModuleIndexPage</h1>
      {isLoading && <UDLoader />}
      <SongsComponentsCreateForm onSubmit={onCreate} />
      <SongsComponentsSongsList songs={songs} onRemove={onRemove} />
    </div>
  );
};

export default ReduxObservableModuleIndexPage;
