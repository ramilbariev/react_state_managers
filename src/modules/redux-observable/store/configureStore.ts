import { applyMiddleware, createStore, Store } from 'redux';
import reduxObservableReducers, {
  ReduxObservableAction,
  ReduxObservableInitialState,
} from './reducers';
import { createEpicMiddleware } from 'redux-observable';
import reduxObservableEpics from './epics';

const epicMiddleware = createEpicMiddleware();

export default function configureStore(preloadedState: ReduxObservableInitialState) {
  const store: Store<ReduxObservableInitialState, ReduxObservableAction> & { dispatch: any } =
    createStore(reduxObservableReducers, preloadedState, applyMiddleware(epicMiddleware));
  epicMiddleware.run(reduxObservableEpics);
  return store;
}
