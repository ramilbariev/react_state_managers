import configureStore from './configureStore';

export const reduxObservableStore = configureStore({
  songs: [],
  detail: undefined,
  isLoading: false,
});

export type ReduxObservableDispatch = typeof reduxObservableStore.dispatch;
export type ReduxObservableState = ReturnType<typeof reduxObservableStore['getState']>;
export default reduxObservableStore;
