import { createSelector } from 'reselect';
import { ReduxObservableState } from '.';

export const reduxObservableStoreSelector = (state: ReduxObservableState) => state;

export const reduxObservableSongsSelector = createSelector(
  reduxObservableStoreSelector,
  (state) => state.songs
);

export const reduxObservableSongDetailSelector = createSelector(
  reduxObservableStoreSelector,
  (state) => state.detail
);

export const reduxObservableIsLoadingSelector = createSelector(
  reduxObservableStoreSelector,
  (state) => state.isLoading
);
