import { action, ActionCreator } from 'typesafe-actions';
import { Song } from 'modules/songs/domain/interfaces/Song';

export enum ReduxObservableActions {
  LOAD_PENDING = 'LOAD_PENDING',
  LOAD_FULFILLED = 'LOAD_FULFILLED',
  LOAD_REJECTED = 'LOAD_REJECTED',
  LOAD_BY_ID_PENDING = 'LOAD_BY_ID_PENDING',
  LOAD_BY_ID_FULFILLED = 'LOAD_BY_ID_FULFILLED',
  LOAD_BY_ID_REJECTED = 'LOAD_BY_ID_REJECTED',
  CREATE_PENDING = 'CREATE_PENDING',
  CREATE_REJECTED = 'CREATE_REJECTED',
  CREATE_FULFILLED = 'CREATE_FULFILLED',
  REMOVE_PENDING = 'REMOVE_PENDING',
  REMOVE_REJECTED = 'REMOVE_REJECTED',
  REMOVE_FULFILLED = 'REMOVE_FULFILLED',
  UPDATE_PENDING = 'UPDATE_PENDING',
  UPDATE_REJECTED = 'UPDATE_REJECTED',
  UPDATE_FULFILLED = 'UPDATE_FULFILLED',
  LOADING_PENDING = 'LOADING_PENDING',
  LOADING_REJECTED = 'LOADING_REJECTED',
  LOADING_FULFILLED = 'LOADING_FULFILLED',
}

const actionFlowCreator = (
  pending: ActionCreator,
  fulfilled: ActionCreator,
  rejected: ActionCreator
) => ({ pending, fulfilled, rejected });

const load = actionFlowCreator(
  () => action(ReduxObservableActions.LOAD_PENDING),
  (songs: Song[]) => action(ReduxObservableActions.LOAD_FULFILLED, songs),
  (error: any) => action(ReduxObservableActions.LOADING_REJECTED, error)
);
const loadById = actionFlowCreator(
  (id: number) => action(ReduxObservableActions.LOAD_BY_ID_PENDING, id),
  (song: Song | undefined) => action(ReduxObservableActions.LOAD_BY_ID_FULFILLED, song),
  (error: any) => action(ReduxObservableActions.LOAD_BY_ID_REJECTED, error)
);

const create = actionFlowCreator(
  (data: Omit<Song, 'id'>) => action(ReduxObservableActions.CREATE_PENDING, data),
  (song: Song) => action(ReduxObservableActions.CREATE_FULFILLED, song),
  (error: any) => action(ReduxObservableActions.CREATE_REJECTED, error)
);

const actionsCreators = {
  load,
  loadById,
  create,
};

export default actionsCreators;
