import songsRepository from 'modules/songs/domain/repositories/songsRepository';
import { combineEpics, Epic } from 'redux-observable';
import { mergeMap, filter, map } from 'rxjs/operators';
import { from } from 'rxjs';
import actions, { ReduxObservableActions } from './actions/actionCreators';
import { ReduxObservableAction } from './reducers';
import { isOfType } from 'typesafe-actions';

// const load: Epic<ReduxObservableAction> = (action$) =>
const load: any = (action$: any) =>
  action$.pipe(
    filter(isOfType(ReduxObservableActions.LOAD_PENDING)),
    mergeMap(() => from(songsRepository.load()).pipe(map((songs) => actions.load.fulfilled(songs))))
  );

const loadById: any = (action$: any) =>
  action$.pipe(
    filter(isOfType(ReduxObservableActions.LOAD_BY_ID_PENDING)),
    mergeMap((action: any) =>
      from(songsRepository.loadById(action.payload)).pipe(
        map((song) => actions.loadById.fulfilled(song))
      )
    )
  );

const reduxObservableEpics = combineEpics(load, loadById) as any;

export default reduxObservableEpics;
