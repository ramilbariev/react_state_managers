import produce from 'immer';
import { Song } from 'modules/songs/domain/interfaces/Song';
import { ReduxObservableActions } from './actions/actionCreators';

export type ReduxObservableInitialState = {
  songs: Song[];
  detail: Song | undefined;
  isLoading: boolean;
};

export type ReduxObservableAction = { payload: any; type: ReduxObservableActions };

const reduxObservableReducers = (
  state: ReduxObservableInitialState = { songs: [], detail: undefined, isLoading: false },
  action: ReduxObservableAction
) => {
  switch (action.type) {
    case ReduxObservableActions.LOAD_PENDING:
      return produce(state, (draftState) => {
        draftState.isLoading = true;
      });
    case ReduxObservableActions.LOAD_FULFILLED:
      return produce(state, (draftState) => {
        draftState.songs = action.payload;
        draftState.isLoading = false;
      });
    case ReduxObservableActions.LOAD_REJECTED:
      return produce(state, (draftState) => {
        draftState.isLoading = false;
      });
    case ReduxObservableActions.LOAD_BY_ID_PENDING:
      return produce(state, (draftState) => {
        draftState.isLoading = true;
      });
    case ReduxObservableActions.LOAD_BY_ID_FULFILLED:
      return produce(state, (draftState) => {
        draftState.detail = action.payload;
        draftState.isLoading = false;
      });
    case ReduxObservableActions.LOAD_BY_ID_REJECTED:
      return produce(state, (draftState) => {
        draftState.isLoading = false;
      });

    default:
      return state;
  }
};

export default reduxObservableReducers;
