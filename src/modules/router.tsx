import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ReduxExtendedModuleRoutes from './redux-extended';
import ReduxJSTookitModuleRoutes from './redux-js-toolkit';
import reduxJSToolkitStore from './redux-js-toolkit/store';
import reduxExtendedStore from './redux-extended/store';
import ReduxJSToolkitSongsIndexPage from './redux-js-toolkit/ui/pages';
import ReduxJSToolkitSongDetailPage from './redux-js-toolkit/ui/pages/detail';
import Navigation from './songs/ui/components/navigation';
import ReduxExtendedSongsPagesDetailPage from './redux-extended/ui/pages/detail';
import ReduxExtendedSongsPagesListPage from './redux-extended/ui/pages';
import MobxSongsIndexPage from './mobx/ui/pages';
import mobxSongsStore from './mobx/store';
import MobxSongsDetailPage from './mobx/ui/pages/detail';
import EffectorModuleSongsIndexPage from './effector/ui/pages';
import EffectorModuleSongDetailPage from './effector/ui/pages/detail';
import SelfWrittenSongDetailPage from './self-written/ui/pages/detail';
import SelfWrittenSongsIndexPage from './self-written/ui/pages';
import ReduxObservableModuleIndexPage from './redux-observable/ui/pages';
import ReduxObservableModuleSongDetailPage from './redux-observable/ui/pages/detail';
import reduxObservableStore from './redux-observable/store';

const AppRouter = () => {
  return (
    <Router>
      <Navigation />
      <Switch>
        <Route path="/toolkit/:id">
          <Provider store={reduxJSToolkitStore}>
            <ReduxJSToolkitSongDetailPage />
          </Provider>
        </Route>
        <Route path="/toolkit">
          <Provider store={reduxJSToolkitStore}>
            <ReduxJSToolkitSongsIndexPage />
          </Provider>
        </Route>
        <Route path="/mobx/:id">
          <MobxSongsDetailPage store={mobxSongsStore} />
        </Route>
        <Route path="/mobx">
          <MobxSongsIndexPage store={mobxSongsStore} />
        </Route>
        <Route path="/effector/:id">
          <EffectorModuleSongDetailPage />
        </Route>
        <Route path="/effector">
          <EffectorModuleSongsIndexPage />
        </Route>
        <Route path="/self-written/:id">
          <SelfWrittenSongDetailPage />
        </Route>
        <Route path="/self-written">
          <SelfWrittenSongsIndexPage />
        </Route>
        <Route path="/redux-observable/:id">
          <Provider store={reduxObservableStore}>
            <ReduxObservableModuleSongDetailPage />
          </Provider>
        </Route>
        <Route path="/redux-observable">
          <Provider store={reduxObservableStore}>
            <ReduxObservableModuleIndexPage />
          </Provider>
        </Route>
        <Route path="/redux-extended/:id">
          <Provider store={reduxExtendedStore}>
            <ReduxExtendedSongsPagesDetailPage />
          </Provider>
        </Route>
        <Route path="/redux-extended">
          <Provider store={reduxExtendedStore}>
            <ReduxExtendedSongsPagesListPage />
          </Provider>
        </Route>
        <Route path="/">
          <h1>Main</h1>
        </Route>
      </Switch>
    </Router>
  );
};

export default AppRouter;
