import React from 'react';
import { Provider } from 'react-redux';
import { Route } from 'react-router-dom';
import reduxJSToolkitStore from './store';
import ReduxJSToolkitSongsIndexPage from './ui/pages';
import ReduxJSToolkitSongDetailPage from './ui/pages/detail';

type Props = {};

const ReduxJSTookitModuleRoutes = (props: Props) => {
  return (
    <>
      <Route path="/toolkit/:id">
        <Provider store={reduxJSToolkitStore}>
          <ReduxJSToolkitSongDetailPage />
        </Provider>
      </Route>
      <Route path="/toolkit">
        <Provider store={reduxJSToolkitStore}>
          <ReduxJSToolkitSongsIndexPage />
        </Provider>
      </Route>
    </>
  );
};

export default ReduxJSTookitModuleRoutes;
