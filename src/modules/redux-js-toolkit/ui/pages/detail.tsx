import {
  reduxJSToolkitSongDetailSelector,
  reduxJSToolkitIsLoadingSelector,
} from 'modules/redux-js-toolkit/store/selector';
import actions from 'modules/redux-js-toolkit/store/actions';
import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import UDLoader from 'modules/ud-ui/components/ud-loader';
import SongsComponentsEditForm from 'modules/songs/ui/components/edit-form';

type Props = {};

const ReduxJSToolkitSongDetailPage = (props: Props) => {
  const { id = 1 } = useParams() as { id: number | undefined };
  const dispatch = useDispatch();
  const song = useSelector(reduxJSToolkitSongDetailSelector);
  const isLoading = useSelector(reduxJSToolkitIsLoadingSelector);
  useEffect(() => {
    dispatch(actions.loadById(+id));
  }, [id]);
  const onSubmit = useCallback(
    (data) => {
      dispatch(actions.update(data));
    },
    [dispatch]
  );
  return (
    <div>
      <h1>ReduxJSToolkitSongDetailPage</h1>
      {isLoading && <UDLoader />}
      <h1>Edit</h1>
      {song && <SongsComponentsEditForm song={song} onSubmit={onSubmit} />}
    </div>
  );
};

export default ReduxJSToolkitSongDetailPage;
