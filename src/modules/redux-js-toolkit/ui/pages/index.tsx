import {
  reduxJSToolkitSongsSelector,
  reduxJSToolkitIsLoadingSelector,
} from 'modules/redux-js-toolkit/store/selector';
import actions from 'modules/redux-js-toolkit/store/actions';
import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import UDLoader from 'modules/ud-ui/components/ud-loader';
import SongsComponentsCreateForm from 'modules/songs/ui/components/create-form';
import SongsComponentsSongsList from 'modules/songs/ui/components/list';

type Props = {};

const ReduxJSToolkitSongsIndexPage = (props: Props) => {
  const dispatch = useDispatch();
  const songs = useSelector(reduxJSToolkitSongsSelector);
  const isLoading = useSelector(reduxJSToolkitIsLoadingSelector);
  useEffect(() => {
    dispatch(actions.load());
  }, [dispatch]);
  const onRemove = useCallback((song) => {
    dispatch(actions.remove(song));
  }, []);
  const onCreate = useCallback(
    (data) => {
      dispatch(actions.create(data));
    },
    [dispatch]
  );
  return (
    <div>
      <h1>ReduxJSToolkitSongsIndexPage</h1>
      {isLoading && <UDLoader />}
      <SongsComponentsCreateForm onSubmit={onCreate} />
      <SongsComponentsSongsList songs={songs} onRemove={onRemove} />
    </div>
  );
};

export default ReduxJSToolkitSongsIndexPage;
