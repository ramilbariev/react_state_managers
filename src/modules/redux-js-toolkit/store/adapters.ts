import { createEntityAdapter } from '@reduxjs/toolkit';
import { Song } from 'modules/songs/domain/interfaces/Song';

export const reduxJSToolkitSongsAdapter = createEntityAdapter<Song>();
