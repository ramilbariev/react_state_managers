import { combineReducers, configureStore } from '@reduxjs/toolkit';

import * as reducers from './reducers';

const reducer = combineReducers(reducers);

const reduxJSToolkitStore = configureStore({
  devTools: process.env.NODE_ENV === 'development',
  reducer,
});

export default reduxJSToolkitStore;
export type ReduxJSToolkitStore = ReturnType<typeof reduxJSToolkitStore['getState']>;
export type ReduxJSToolkitDispatch = typeof reduxJSToolkitStore.dispatch;
