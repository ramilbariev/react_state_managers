import { ReduxJSToolkitStore } from '.';
import { ReduxJSToolkitSongsStore } from './slice';
import { createSelector } from '@reduxjs/toolkit';
import { reduxJSToolkitSongsAdapter } from './adapters';

const reduxJSToolkitSongsStoreSelector = (store: ReduxJSToolkitStore): ReduxJSToolkitSongsStore =>
  store.reduxJSToolkitSongsStore;

export const reduxJSToolkitSongsSelector = createSelector(
  reduxJSToolkitSongsStoreSelector,
  (state) => {
    return reduxJSToolkitSongsAdapter.getSelectors().selectAll(state);
  }
);

export const reduxJSToolkitSongDetailSelector = createSelector(
  reduxJSToolkitSongsStoreSelector,
  (state) => {
    return state.detail;
  }
);

export const reduxJSToolkitIsLoadingSelector = createSelector(
  reduxJSToolkitSongsStoreSelector,
  (state) => {
    return state.isLoading;
  }
);
