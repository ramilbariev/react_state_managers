import { createAsyncThunk } from '@reduxjs/toolkit';
import { Song } from 'modules/songs/domain/interfaces/Song';
import songsRepository from 'modules/songs/domain/repositories/songsRepository';

const PREFIX = 'ReduxJSToolkitSongsStore';

const load = createAsyncThunk<Song[], void>(`${PREFIX}/load`, () => {
  return songsRepository.load();
});

const loadById = createAsyncThunk<Song | undefined, number>(`${PREFIX}/loadById`, (id) => {
  return songsRepository.loadById(id);
});

const create = createAsyncThunk<Song, Omit<Song, 'id'>>(`${PREFIX}/create`, (data) => {
  return songsRepository.create(data);
});

const update = createAsyncThunk<Song, Song>(`${PREFIX}/update`, (song) => {
  return songsRepository.update(song);
});

const remove = createAsyncThunk<Song, Song>(`${PREFIX}/remove`, async (song) => {
  await songsRepository.delete(song);
  return song;
});

const reduxJSToolkitSongsStoreActions = {
  load,
  loadById,
  create,
  remove,
  update,
};

export default reduxJSToolkitSongsStoreActions;
