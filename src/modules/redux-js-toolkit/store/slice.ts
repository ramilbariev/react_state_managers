import { createSlice } from '@reduxjs/toolkit';
import { reduxJSToolkitSongsAdapter } from './adapters';
import actions from './actions';
import { Song } from 'modules/songs/domain/interfaces/Song';

const slice = createSlice({
  name: 'reduxJSToolkitSlice',
  initialState: reduxJSToolkitSongsAdapter.getInitialState({
    isLoading: false,
    detail: undefined as Song | undefined,
  }),
  reducers: {},
  extraReducers: (builder) => {
    // load
    builder.addCase(actions.load.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(actions.load.fulfilled, (state, { payload }) => {
      state.isLoading = false;
      reduxJSToolkitSongsAdapter.setAll(state, payload);
    });
    builder.addCase(actions.load.rejected, (state) => {
      state.isLoading = false;
    });
    // loadById
    builder.addCase(actions.loadById.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(actions.loadById.fulfilled, (state, { payload }) => {
      state.isLoading = false;
      state.detail = payload;
    });
    builder.addCase(actions.loadById.rejected, (state) => {
      state.isLoading = false;
    });
    // create
    builder.addCase(actions.create.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(actions.create.fulfilled, (state, { payload }) => {
      state.isLoading = false;
      reduxJSToolkitSongsAdapter.addOne(state, payload);
    });
    builder.addCase(actions.create.rejected, (state) => {
      state.isLoading = false;
    });
    // update
    builder.addCase(actions.update.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(actions.update.fulfilled, (state, { payload }) => {
      state.isLoading = false;
      reduxJSToolkitSongsAdapter.upsertOne(state, payload);
    });
    builder.addCase(actions.update.rejected, (state) => {
      state.isLoading = false;
    });
    // remove
    builder.addCase(actions.remove.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(actions.remove.fulfilled, (state, { payload }) => {
      state.isLoading = false;
      reduxJSToolkitSongsAdapter.removeOne(state, payload.id);
    });
    builder.addCase(actions.remove.rejected, (state) => {
      state.isLoading = false;
    });
  },
});

const reduxJSToolkitSongsStore = slice.reducer;
export type ReduxJSToolkitSongsStore = ReturnType<typeof slice['reducer']>;
export default reduxJSToolkitSongsStore;
