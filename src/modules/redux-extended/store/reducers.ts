import produce from 'immer';
import { Song } from 'modules/songs/domain/interfaces/Song';
import { ReduxExtendedActions } from './actions/actionCreators';

export type ReduxExtendedInitialState = {
  songs: Song[];
  detail: Song | undefined;
  isLoading: boolean;
};
export type ReduxExtendedAction = { payload: any; type: ReduxExtendedActions };

const reduxExtendedReducer = (
  state: ReduxExtendedInitialState = { songs: [], detail: undefined, isLoading: false },
  action: ReduxExtendedAction
) => {
  switch (action.type) {
    case ReduxExtendedActions.LOAD:
      return produce(state, (draftState) => {
        draftState.songs = action.payload;
      });
    case ReduxExtendedActions.LOAD_BY_ID:
      return produce(state, (draftState) => {
        draftState.detail = action.payload;
      });
    case ReduxExtendedActions.CREATE:
      return produce(state, (draftState) => {
        draftState.songs.push(action.payload);
      });
    case ReduxExtendedActions.UPDATE:
      return produce(state, (draftState) => {
        const songs = draftState.songs;
        const songToUpdate = songs.findIndex((song) => song.id === action.payload.id);
        songs[songToUpdate] = action.payload;
        draftState.songs = songs;
      });
    case ReduxExtendedActions.REMOVE:
      return produce(state, (draftState) => {
        const songs = draftState.songs;
        draftState.songs = songs.filter((song) => song.id !== action.payload.id);
      });
    case ReduxExtendedActions.LOADING:
      return produce(state, (draftState) => {
        draftState.isLoading = action.payload;
      });
    default:
      return state;
  }
};

export default reduxExtendedReducer;
