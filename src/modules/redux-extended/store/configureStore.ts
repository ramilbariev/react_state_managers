import { applyMiddleware, createStore, Store } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer, { ReduxExtendedInitialState, ReduxExtendedAction } from './reducers';

export default function configureStore(preloadedState: ReduxExtendedInitialState) {
  const store: Store<ReduxExtendedInitialState, ReduxExtendedAction> & { dispatch: any } =
    createStore(rootReducer, preloadedState, applyMiddleware(thunkMiddleware));
  return store;
}
