import { createSelector } from 'reselect';
import { ReduxExtendedState } from '.';

export const reduxExtendedStoreSelector = (state: ReduxExtendedState) => state;

export const reduxExtendedSongsSelector = createSelector(
  reduxExtendedStoreSelector,
  (state) => state.songs
);

export const reduxExtendedSongDetailSelector = createSelector(
  reduxExtendedStoreSelector,
  (state) => state.detail
);

export const reduxExtendedIsLoadingSelector = createSelector(
  reduxExtendedStoreSelector,
  (state) => state.isLoading
);
