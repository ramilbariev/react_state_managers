import { Song } from 'modules/songs/domain/interfaces/Song';

export enum ReduxExtendedActions {
  LOAD = 'LOAD',
  LOAD_BY_ID = 'LOAD_BY_ID',
  CREATE = 'CREATE',
  REMOVE = 'REMOVE',
  UPDATE = 'UPDATE',
  LOADING = 'LOADING',
}

const load = (songs: Song[]) => ({ type: ReduxExtendedActions.LOAD, payload: songs });
const loadById = (song: Song | undefined) => ({
  type: ReduxExtendedActions.LOAD_BY_ID,
  payload: song,
});
const create = (data: Omit<Song, 'id'>) => ({
  type: ReduxExtendedActions.CREATE,
  payload: data,
});
const update = (data: Song) => ({ type: ReduxExtendedActions.UPDATE, payload: data });
const remove = (song: Song) => ({ type: ReduxExtendedActions.REMOVE, payload: song });
const loading = (isLoading: boolean) => ({
  type: ReduxExtendedActions.LOADING,
  payload: isLoading,
});

const actionsCreators = {
  load,
  loadById,
  create,
  update,
  remove,
  loading,
};

export default actionsCreators;
