import { Song } from 'modules/songs/domain/interfaces/Song';
import songsRepository from 'modules/songs/domain/repositories/songsRepository';
import { ReduxExtendedDispatch } from '..';
import actionsCreators from './actionCreators';

const load = () => {
  return (dispatch: ReduxExtendedDispatch) => {
    dispatch(actionsCreators.loading(true));
    return songsRepository
      .load()
      .then((songs) => {
        dispatch(actionsCreators.load(songs));
      })
      .finally(() => {
        dispatch(actionsCreators.loading(false));
      });
  };
};

const loadById = (id: number) => {
  return (dispatch: ReduxExtendedDispatch) => {
    dispatch(actionsCreators.loading(true));
    return songsRepository
      .loadById(id)
      .then((song) => {
        dispatch(actionsCreators.loadById(song));
      })
      .finally(() => {
        dispatch(actionsCreators.loading(false));
      });
  };
};

const create = (data: Omit<Song, 'id'>) => {
  return (dispatch: ReduxExtendedDispatch) => {
    dispatch(actionsCreators.loading(true));
    return songsRepository
      .create(data)
      .then((song) => {
        dispatch(actionsCreators.create(song));
      })
      .finally(() => {
        dispatch(actionsCreators.loading(false));
      });
  };
};

const update = (data: Song) => {
  return (dispatch: ReduxExtendedDispatch) => {
    dispatch(actionsCreators.loading(true));
    return songsRepository
      .update(data)
      .then((song) => dispatch(actionsCreators.update(song)))
      .finally(() => {
        dispatch(actionsCreators.loading(false));
      });
  };
};

const remove = (song: Song) => {
  return (dispatch: ReduxExtendedDispatch) => {
    dispatch(actionsCreators.loading(true));
    return songsRepository
      .delete(song)
      .then(() => dispatch(actionsCreators.remove(song)))
      .finally(() => {
        dispatch(actionsCreators.loading(false));
      });
  };
};

const reduxExtendedActions = {
  load,
  create,
  loadById,
  update,
  remove,
};
export default reduxExtendedActions;
