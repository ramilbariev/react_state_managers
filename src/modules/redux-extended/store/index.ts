import configureStore from './configureStore';

export const store = configureStore({ songs: [], detail: undefined, isLoading: false });

export type ReduxExtendedDispatch = typeof store.dispatch;
export type ReduxExtendedState = ReturnType<typeof store['getState']>;
export default store;
