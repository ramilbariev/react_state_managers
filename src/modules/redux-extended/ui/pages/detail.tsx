import React, { useCallback, useEffect } from 'react';
import {
  reduxExtendedSongDetailSelector,
  reduxExtendedIsLoadingSelector,
} from 'modules/redux-extended/store/selectors';
import SongsComponentsEditForm from 'modules/songs/ui/components/edit-form';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import reduxExtendedActions from 'modules/redux-extended/store/actions';
import UDLoader from 'modules/ud-ui/components/ud-loader';

type Props = {};

const ReduxExtendedSongsPagesDetailPage = (props: Props) => {
  const { id = 1 } = useParams() as { id: number | undefined };
  const dispatch = useDispatch();
  const song = useSelector(reduxExtendedSongDetailSelector);
  const isLoading = useSelector(reduxExtendedIsLoadingSelector);
  useEffect(() => {
    dispatch(reduxExtendedActions.loadById(+id));
  }, [id]);
  const onSubmit = useCallback(
    (data) => {
      dispatch(reduxExtendedActions.update(data));
    },
    [dispatch]
  );
  return (
    <div>
      <h1>ReduxExtendedSongsPagesDetailPage</h1>
      {isLoading && <UDLoader />}
      <h1>Edit</h1>
      {song && <SongsComponentsEditForm song={song} onSubmit={onSubmit} />}
    </div>
  );
};

export default ReduxExtendedSongsPagesDetailPage;
