import React, { useCallback, useEffect } from 'react';
import SongsComponentsSongsList from 'modules/songs/ui/components/list';
import { useDispatch, useSelector } from 'react-redux';
import reduxExtendedActions from 'modules/redux-extended/store/actions';
import {
  reduxExtendedIsLoadingSelector,
  reduxExtendedSongsSelector,
} from 'modules/redux-extended/store/selectors';
import SongsComponentsCreateForm from 'modules/songs/ui/components/create-form';
import UDLoader from 'modules/ud-ui/components/ud-loader';

const ReduxExtendedSongsPagesListPage = () => {
  const dispatch = useDispatch();
  const songs = useSelector(reduxExtendedSongsSelector);
  const isLoading = useSelector(reduxExtendedIsLoadingSelector);
  useEffect(() => {
    dispatch(reduxExtendedActions.load());
  }, [dispatch]);
  const onRemove = useCallback((song) => {
    dispatch(reduxExtendedActions.remove(song));
  }, []);
  const onCreate = useCallback(
    (data) => {
      dispatch(reduxExtendedActions.create(data));
    },
    [dispatch]
  );
  return (
    <div>
      <h1>ReduxExtendedSongsPagesListPage</h1>
      {isLoading && <UDLoader />}
      <SongsComponentsCreateForm onSubmit={onCreate} />
      <SongsComponentsSongsList songs={songs} onRemove={onRemove} />
    </div>
  );
};

export default ReduxExtendedSongsPagesListPage;
