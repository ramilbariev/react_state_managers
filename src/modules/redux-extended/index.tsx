import React from 'react';
import { Provider } from 'react-redux';
import { Route } from 'react-router-dom';
import store from './store';
import ReduxExtendedSongsPagesListPage from './ui/pages';
import ReduxExtendedSongsPagesDetailPage from './ui/pages/detail';

const ReduxExtendedModuleRoutes = () => {
  return (
    <>
      <Route path="/redux-extended/:id">
        <Provider store={store}>
          <ReduxExtendedSongsPagesDetailPage />
        </Provider>
      </Route>
      <Route path="/redux-extended">
        <Provider store={store}>
          <ReduxExtendedSongsPagesListPage />
        </Provider>
      </Route>
    </>
  );
};

export default ReduxExtendedModuleRoutes;
