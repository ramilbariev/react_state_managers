import React from 'react';
import './styles.css';

type Props = {};

const UDLoader = (props: Props) => {
  return (
    <div className="lds-container">
      <div className="lds-spinner">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  );
};

export default UDLoader;
