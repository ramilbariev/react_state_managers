import localforage from 'localforage';

class StorageService {
  setItem<T>(key: string, value: T): Promise<T> {
    return localforage.setItem(key, value);
  }
  getItem<T>(key: string): Promise<T | null> {
    return localforage.getItem(key);
  }
  deleteItem(key: string): Promise<void> {
    return localforage.removeItem(key);
  }
}

const storageService = new StorageService();
export default storageService;
