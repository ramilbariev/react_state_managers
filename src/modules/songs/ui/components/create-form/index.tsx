import React, { useCallback, useRef } from 'react';
import { Song } from 'modules/songs/domain/interfaces/Song';

type Props = {
  onSubmit: (data: Omit<Song, 'id'>) => any;
};

const SongsComponentsCreateForm = (props: Props) => {
  const formRef = useRef<any>();
  const resetForm = useCallback(() => {
    formRef.current.reset();
  }, [formRef]);
  const onSubmit = useCallback((e) => {
    e.preventDefault();
    const name = e.target.name.value;
    const author = e.target.author.value;
    const release_year = e.target.release_year.value;
    const data = {
      name,
      author,
      release_year,
    };
    props.onSubmit(data);
    resetForm();
  }, []);

  return (
    <form onSubmit={onSubmit} ref={formRef}>
      <input name="name"></input>
      <input name="author"></input>
      <input name="release_year" type="number" min="2000" max={new Date().getFullYear()}></input>
      <button type="submit">Create</button>
    </form>
  );
};

export default SongsComponentsCreateForm;
