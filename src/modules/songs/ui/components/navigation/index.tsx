import { NavLink } from 'react-router-dom';

const navLinks = [
  { label: 'Main', to: '/' },
  { label: 'Redux extended', to: '/redux-extended' },
  { label: 'Redux JS Toolkit', to: '/toolkit' },
  { label: 'MobX', to: '/mobx' },
  { label: 'Effector', to: '/effector' },
  { label: 'Self Written', to: '/self-written' },
  { label: 'Redux Observable', to: '/redux-observable' },
];

const Navigation = () => {
  return (
    <nav>
      {navLinks.map((link) => (
        <NavLink
          to={link.to}
          activeStyle={{ fontWeight: 600 }}
          style={{ marginRight: '8px' }}
          key={link.to}
          exact
        >
          {link.label}
        </NavLink>
      ))}
    </nav>
  );
};

export default Navigation;
