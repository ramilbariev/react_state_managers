import React, { useCallback } from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { Song } from '../../../domain/interfaces/Song';

type Props = {
  song: Song;
  onRemove: (song: Song) => any;
};

const SongsComponentsSongsListItem = (props: Props) => {
  const { song, onRemove } = props;
  const { location } = useHistory();
  const onRemoveSong = useCallback(() => {
    onRemove(song);
  }, [song]);
  return (
    <li>
      {song.name} {song.author} ({song.release_year})
      <NavLink to={`${location.pathname}/${song.id}`}>Редактировать</NavLink>
      <button onClick={onRemoveSong}>Удалить</button>
    </li>
  );
};

export default SongsComponentsSongsListItem;
