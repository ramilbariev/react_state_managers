import React from 'react';
import { Song } from 'modules/songs/domain/interfaces/Song';
import SongsComponentsSongsListItem from '../list-item';

type Props = {
  songs: Song[];
  onRemove: (song: Song) => any;
};

const SongsComponentsSongsList = (props: Props) => {
  return (
    <ul>
      {props.songs.map((song) => (
        <SongsComponentsSongsListItem key={song.id} song={song} onRemove={props.onRemove} />
      ))}
    </ul>
  );
};

export default SongsComponentsSongsList;
