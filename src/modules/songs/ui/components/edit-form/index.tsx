import React, { useCallback } from 'react';
import { Song } from 'modules/songs/domain/interfaces/Song';

type Props = {
  song: Song;
  onSubmit: (data: Song) => any;
};

const SongsComponentsEditForm = (props: Props) => {
  const { song } = props;
  const onSubmit = useCallback(
    (e) => {
      e.preventDefault();
      const id = e.target.id.value;
      const name = e.target.name.value;
      const author = e.target.author.value;
      const release_year = e.target.release_year.value;
      const data = {
        id: +id,
        name,
        author,
        release_year,
      };
      props.onSubmit(data);
    },
    [song]
  );
  return (
    <form onSubmit={onSubmit} key={song.id}>
      <input name="id" hidden defaultValue={song.id}></input>
      <label>Name</label>
      <input name="name" defaultValue={song.name}></input>
      <label>Author</label>
      <input name="author" defaultValue={song.author}></input>
      <label>Release year</label>
      <input
        name="release_year"
        type="number"
        min="2000"
        max={new Date().getFullYear()}
        defaultValue={song.release_year}
      ></input>
      <button type="submit">Update</button>
    </form>
  );
};

// export default SongsComponentsEditForm;
export default React.memo(SongsComponentsEditForm, (p, n) => p.song.id === n.song.id);
