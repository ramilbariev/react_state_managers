import React, { ReactChild } from 'react';

type Props = {
  children: ReactChild;
};

const ListLayout = (props: Props) => {
  return <div>{props.children}</div>;
};

export default ListLayout;
