import { ReactChild } from 'react';

type Props = {
  children: ReactChild;
};

const MainLayout = (props: Props) => {
  return <div>{props.children}</div>;
};

export default MainLayout;
