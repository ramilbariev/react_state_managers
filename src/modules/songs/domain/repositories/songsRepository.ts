import storageService from 'modules/core/services/storageService';
import { Song } from '../interfaces/Song';
import { mockSongs } from './mock';

class SongsRepository {
  private readonly KEY = 'songs';
  constructor() {
    (async () => {
      // await storageService.deleteItem(this.KEY);
      const isEmpty = !Boolean(await storageService.getItem(this.KEY));
      if (isEmpty) {
        await storageService.setItem(this.KEY, mockSongs);
      }
    })();
  }

  async load(): Promise<Song[]> {
    console.log('load');

    await new Promise((r, rj) =>
      setTimeout(() => {
        r(null);
      }, 1000)
    );
    const list = await storageService.getItem<Song[]>(this.KEY);
    if (list === null) {
      return [];
    }
    return list;
  }

  async loadById(id: number): Promise<Song | undefined> {
    const list = (await storageService.getItem<Song[]>(this.KEY)) || [];
    return list.find((song) => song.id === id);
  }
  async create(data: Omit<Song, 'id'>): Promise<Song> {
    const id = Date.now();
    const list = await this.load();
    const created = { id, ...data };
    await storageService.setItem(this.KEY, [...list, created]);
    return created;
  }
  async delete(songToDelete: Song): Promise<void> {
    const list = await this.load();
    const updatedList = list.filter((song) => song.id !== songToDelete.id);
    await storageService.setItem(this.KEY, updatedList);
  }
  async update(data: Song): Promise<Song> {
    const list = await this.load();
    const candidateIndex = list.findIndex((song) => song.id === data.id);
    list.splice(candidateIndex, 1, data);
    await storageService.setItem(this.KEY, list);
    return data;
  }
}

const songsRepository = new SongsRepository();
export default songsRepository;
