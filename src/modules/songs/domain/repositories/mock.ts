import { Song } from '../interfaces/Song';

export const mockSongs = [
  {
    id: 1,
    name: 'Some name',
    author: 'Some author',
    release_year: '2021',
  },
] as Song[];
