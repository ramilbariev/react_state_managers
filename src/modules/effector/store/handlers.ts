import { Song } from 'modules/songs/domain/interfaces/Song';
import { EffectorSongsStore } from './EffectorSongsStore';

const update = (state: EffectorSongsStore, updated: Song) => {
  const songsWithoutUpdated = state.songs.filter((song) => song.id !== updated.id);
  return { ...state, songs: [...songsWithoutUpdated, updated] };
};

const load = (state: EffectorSongsStore, songs: Song[]) => ({ ...state, songs });
const loadById = (state: EffectorSongsStore, song: Song | undefined) => ({
  ...state,
  detail: song,
});
const create = (state: EffectorSongsStore, song: Song) => ({
  ...state,
  songs: [...state.songs, song],
});
const remove = (state: EffectorSongsStore, deleted: Song) => ({
  ...state,
  songs: state.songs.filter((song) => song.id !== deleted.id),
});

const effectorSongsStoreHandlers = {
  load,
  update,
  create,
  remove,
  loadById,
};

export default effectorSongsStoreHandlers;
