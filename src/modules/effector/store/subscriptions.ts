import effectorSongsStoreEffects from './effects';
import effectorSongsStoreHandlers from './handlers';
import $effectorSongsStore from './store';

// load
$effectorSongsStore.on(effectorSongsStoreEffects.load.doneData, effectorSongsStoreHandlers.load);
$effectorSongsStore.on(effectorSongsStoreEffects.load.pending, (state, pending) => {
  return { ...state, isLoading: pending };
});

// loadById
$effectorSongsStore.on(
  effectorSongsStoreEffects.loadById.doneData,
  effectorSongsStoreHandlers.loadById
);
$effectorSongsStore.on(effectorSongsStoreEffects.loadById.pending, (state, pending) => {
  return { ...state, isLoading: pending };
});

// create
$effectorSongsStore.on(
  effectorSongsStoreEffects.create.doneData,
  effectorSongsStoreHandlers.create
);
$effectorSongsStore.on(effectorSongsStoreEffects.create.pending, (state, pending) => {
  return { ...state, isLoading: pending };
});

// remove
$effectorSongsStore.on(
  effectorSongsStoreEffects.remove.doneData,
  effectorSongsStoreHandlers.remove
);
$effectorSongsStore.on(effectorSongsStoreEffects.remove.pending, (state, pending) => {
  return { ...state, isLoading: pending };
});

// update
$effectorSongsStore.on(
  effectorSongsStoreEffects.update.doneData,
  effectorSongsStoreHandlers.update
);
$effectorSongsStore.on(effectorSongsStoreEffects.update.pending, (state, pending) => {
  return { ...state, isLoading: pending };
});
