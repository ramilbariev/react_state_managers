import { createEffect } from 'effector';
import { Song } from 'modules/songs/domain/interfaces/Song';
import songsRepository from 'modules/songs/domain/repositories/songsRepository';

const load = createEffect(() => songsRepository.load());
const loadById = createEffect((id: number) => songsRepository.loadById(id));
const create = createEffect((data: Omit<Song, 'id'>) => songsRepository.create(data));
const remove = createEffect(async (song: Song) => {
  await songsRepository.delete(song);
  return song;
});
const update = createEffect(async (song: Song) => songsRepository.update(song));

const effectorSongsStoreEffects = {
  load,
  loadById,
  create,
  remove,
  update,
};

export default effectorSongsStoreEffects;
