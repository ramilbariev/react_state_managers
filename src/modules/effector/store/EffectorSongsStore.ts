import { Song } from 'modules/songs/domain/interfaces/Song';

export interface EffectorSongsStore {
  songs: Song[];
  detail: Song | undefined;
  isLoading: boolean;
}
