import { createEvent } from 'effector';
import { Song } from 'modules/songs/domain/interfaces/Song';

const create = createEvent<Omit<Song, 'id'>>('createSong');
const update = createEvent<Song>('updateSong');
const remove = createEvent<Song>('removeSong');
const load = createEvent<Song>('loadSongs');
const loadById = createEvent<Song>('loadByIdSong');

const effectorSongsStoreEvents = {
  create,
  update,
  remove,
  load,
  loadById,
};

export default effectorSongsStoreEvents;
