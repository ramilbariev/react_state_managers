import { createStore } from 'effector';
import { EffectorSongsStore } from './EffectorSongsStore';

const inititalState = {
  songs: [],
  detail: undefined,
  isLoading: false,
};

const $effectorSongsStore = createStore<EffectorSongsStore>(inititalState);
export default $effectorSongsStore;
