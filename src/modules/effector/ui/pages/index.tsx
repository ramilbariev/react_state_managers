import { useStore } from 'effector-react';
import $effectorSongsStore from 'modules/effector/store';
import effectorSongsStoreEffects from 'modules/effector/store/effects';
import SongsComponentsCreateForm from 'modules/songs/ui/components/create-form';
import SongsComponentsSongsList from 'modules/songs/ui/components/list';
import UDLoader from 'modules/ud-ui/components/ud-loader';
import React, { useCallback, useEffect } from 'react';

type Props = {};

const EffectorModuleSongsIndexPage = (props: Props) => {
  useEffect(() => {
    effectorSongsStoreEffects.load();
  }, []);
  const onCreate = useCallback((data) => {
    effectorSongsStoreEffects.create(data);
  }, []);
  const onRemove = useCallback((song) => {
    effectorSongsStoreEffects.remove(song);
  }, []);

  const { isLoading, songs } = useStore($effectorSongsStore);
  return (
    <div>
      <h1>EffectorModuleSongsIndexPage</h1>
      {isLoading && <UDLoader />}
      <SongsComponentsCreateForm onSubmit={onCreate} />
      <SongsComponentsSongsList songs={songs} onRemove={onRemove} />
    </div>
  );
};

export default EffectorModuleSongsIndexPage;
