import React, { useCallback, useEffect } from 'react';
import { useStore } from 'effector-react';
import $effectorSongsStore from 'modules/effector/store';
import UDLoader from 'modules/ud-ui/components/ud-loader';
import { useParams } from 'react-router-dom';
import SongsComponentsEditForm from 'modules/songs/ui/components/edit-form';
import effectorSongsStoreEffects from 'modules/effector/store/effects';

type Props = {};

const EffectorModuleSongDetailPage = (props: Props) => {
  const { id = 1 } = useParams() as { id: number | undefined };
  useEffect(() => {
    effectorSongsStoreEffects.loadById(+id);
  }, [id]);
  const onSubmit = useCallback((data) => {
    effectorSongsStoreEffects.update(data);
  }, []);
  const { detail: song, isLoading } = useStore($effectorSongsStore);
  return (
    <div>
      <h1>EffectorModuleSongDetailPage</h1>
      {isLoading && <UDLoader />}
      <h1>Edit</h1>
      {song && <SongsComponentsEditForm song={song} onSubmit={onSubmit} />}
    </div>
  );
};

export default EffectorModuleSongDetailPage;
