import React, { useCallback, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import UDLoader from 'modules/ud-ui/components/ud-loader';
import SongsComponentsEditForm from 'modules/songs/ui/components/edit-form';
import { useReducer } from 'react';
import { selfWrittenStoreReducer } from 'modules/self-written/store/reducers';
import selfWrittenStoreInitialState from 'modules/self-written/store/initialState';
import songsRepository from 'modules/songs/domain/repositories/songsRepository';
import selfWrittenStoreActionCreators from 'modules/self-written/store/actionCreators';

type Props = {};

const SelfWrittenSongDetailPage = (props: Props) => {
  const { id = 1 } = useParams() as { id: number | undefined };
  const [state, dispatch] = useReducer(selfWrittenStoreReducer, selfWrittenStoreInitialState);
  useEffect(() => {
    dispatch(selfWrittenStoreActionCreators.loading(true));
    songsRepository
      .loadById(+id)
      .then((song) => {
        dispatch(selfWrittenStoreActionCreators.loadById(song));
      })
      .finally(() => {
        dispatch(selfWrittenStoreActionCreators.loading(false));
      });
  }, [id]);
  const onSubmit = useCallback((data) => {
    dispatch(selfWrittenStoreActionCreators.loading(true));
    songsRepository
      .update(data)
      .then((song) => {
        dispatch(selfWrittenStoreActionCreators.update(song));
      })
      .finally(() => {
        dispatch(selfWrittenStoreActionCreators.loading(false));
      });
  }, []);
  const { isLoading, detail: song } = state;
  return (
    <div>
      <h1>SelfWrittenSongDetailPage</h1>
      {isLoading && <UDLoader />}
      <h1>Edit</h1>
      {song && <SongsComponentsEditForm song={song} onSubmit={onSubmit} />}
    </div>
  );
};

export default SelfWrittenSongDetailPage;
