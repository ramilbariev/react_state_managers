import React, { useCallback, useEffect } from 'react';
import SongsComponentsCreateForm from 'modules/songs/ui/components/create-form';
import SongsComponentsSongsList from 'modules/songs/ui/components/list';
import UDLoader from 'modules/ud-ui/components/ud-loader';
import { useReducer } from 'react';
import { selfWrittenStoreReducer } from 'modules/self-written/store/reducers';
import selfWrittenStoreInitialState from 'modules/self-written/store/initialState';
import songsRepository from 'modules/songs/domain/repositories/songsRepository';
import selfWrittenStoreActionCreators from 'modules/self-written/store/actionCreators';

type Props = {};

const SelfWrittenSongsIndexPage = (props: Props) => {
  const [state, dispatch] = useReducer(selfWrittenStoreReducer, selfWrittenStoreInitialState);
  useEffect(() => {
    dispatch(selfWrittenStoreActionCreators.loading(true));
    songsRepository
      .load()
      .then((songs) => {
        dispatch(selfWrittenStoreActionCreators.load(songs));
      })
      .finally(() => {
        dispatch(selfWrittenStoreActionCreators.loading(false));
      });
  }, []);
  const onCreate = useCallback((data) => {
    dispatch(selfWrittenStoreActionCreators.loading(true));
    songsRepository
      .create(data)
      .then((song) => {
        dispatch(selfWrittenStoreActionCreators.create(song));
      })
      .finally(() => {
        dispatch(selfWrittenStoreActionCreators.loading(false));
      });
  }, []);
  const onRemove = useCallback((song) => {
    dispatch(selfWrittenStoreActionCreators.loading(true));
    songsRepository
      .delete(song)
      .then(() => {
        dispatch(selfWrittenStoreActionCreators.remove(song));
      })
      .finally(() => {
        dispatch(selfWrittenStoreActionCreators.loading(false));
      });
  }, []);
  const { isLoading, songs } = state;
  return (
    <div>
      <h1>SelfWrittenSongsIndexPage</h1>
      {isLoading && <UDLoader />}
      <SongsComponentsCreateForm onSubmit={onCreate} />
      <SongsComponentsSongsList songs={songs} onRemove={onRemove} />
    </div>
  );
};

export default SelfWrittenSongsIndexPage;
