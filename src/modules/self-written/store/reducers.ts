import { SelfWrittenStore, SelfWrittenStoreAction, SelfWrittenStoreActionType } from './interfaces';

export function selfWrittenStoreReducer(
  state: SelfWrittenStore,
  action: SelfWrittenStoreAction
): SelfWrittenStore {
  switch (action.type) {
    case SelfWrittenStoreActionType.LOAD:
      return { ...state, songs: action.payload };
    case SelfWrittenStoreActionType.LOAD_BY_ID:
      return { ...state, detail: action.payload };
    case SelfWrittenStoreActionType.CREATE:
      return { ...state, songs: [...state.songs, action.payload] };
    case SelfWrittenStoreActionType.REMOVE:
      return { ...state, songs: state.songs.filter((song) => song.id !== action.payload.id) };
    case SelfWrittenStoreActionType.UPDATE:
      return {
        ...state,
        songs: [...state.songs.filter((song) => song.id !== action.payload.id), action.payload],
        detail: action.payload,
      };
    case SelfWrittenStoreActionType.LOADING:
      return { ...state, isLoading: action.payload };
    default:
      throw new Error();
  }
}
