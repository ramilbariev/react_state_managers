import { Song } from 'modules/songs/domain/interfaces/Song';
import { SelfWrittenStoreAction, SelfWrittenStoreActionType } from './interfaces';

const load = (songs: Song[]): SelfWrittenStoreAction => ({
  type: SelfWrittenStoreActionType.LOAD,
  payload: songs,
});

const loadById = (song: Song | undefined): SelfWrittenStoreAction => ({
  type: SelfWrittenStoreActionType.LOAD_BY_ID,
  payload: song,
});

const loading = (value: boolean): SelfWrittenStoreAction => ({
  type: SelfWrittenStoreActionType.LOADING,
  payload: value,
});

const create = (song: Song): SelfWrittenStoreAction => ({
  type: SelfWrittenStoreActionType.CREATE,
  payload: song,
});

const remove = (song: Song): SelfWrittenStoreAction => ({
  type: SelfWrittenStoreActionType.REMOVE,
  payload: song,
});

const update = (song: Song): SelfWrittenStoreAction => ({
  type: SelfWrittenStoreActionType.UPDATE,
  payload: song,
});

const selfWrittenStoreActionCreators = {
  load,
  loadById,
  loading,
  create,
  remove,
  update,
};

export default selfWrittenStoreActionCreators;
