import songsRepository from 'modules/songs/domain/repositories/songsRepository';
import selfWrittenStoreActionCreators from './actionCreators';

const load = () =>
  songsRepository.load().then((songs) => selfWrittenStoreActionCreators.load(songs));

const selfWrittenStoreActions = {
  load,
};
export default selfWrittenStoreActions;
