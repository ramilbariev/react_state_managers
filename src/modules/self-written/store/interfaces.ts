import { Song } from 'modules/songs/domain/interfaces/Song';

export interface SelfWrittenStore {
  songs: Song[];
  detail: Song | undefined;
  isLoading: boolean;
}

export enum SelfWrittenStoreActionType {
  LOAD = 'LOAD',
  LOADING = 'LOADING',
  LOAD_BY_ID = 'LOAD_BY_ID',
  CREATE = 'CREATE',
  UPDATE = 'UPDATE',
  REMOVE = 'REMOVE',
}

export interface SelfWrittenStoreAction {
  type: SelfWrittenStoreActionType;
  payload: any;
}
