import { SelfWrittenStore } from './interfaces';

const selfWrittenStoreInitialState: SelfWrittenStore = {
  songs: [],
  detail: undefined,
  isLoading: false,
};

export default selfWrittenStoreInitialState;
